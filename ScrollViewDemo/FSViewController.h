//
//  FSViewController.h
//  ScrollViewDemo
//
//  Created by Ho Yan Leung on 2013-08-20.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSViewController : UIViewController
@property (nonatomic,retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic,retain) IBOutlet UITextField *firstTextField;
@property (nonatomic,retain) IBOutlet UITextField *secondTextField;
@property (nonatomic,retain) IBOutlet UITextField *thirdTextField;
-(IBAction)leftButtonClicked:(id)sender;
-(IBAction)rightButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end
