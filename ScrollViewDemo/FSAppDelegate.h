//
//  FSAppDelegate.h
//  ScrollViewDemo
//
//  Created by Ho Yan Leung on 2013-08-20.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
