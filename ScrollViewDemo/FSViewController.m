//
//  FSViewController.m
//  ScrollViewDemo
//
//  Created by Ho Yan Leung on 2013-08-20.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import "FSViewController.h"

@interface FSViewController ()

@end

@implementation FSViewController
@synthesize scrollView;
@synthesize firstTextField,secondTextField,thirdTextField;
@synthesize topConstraint;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a `nib.
    UIView *contentView = [[[NSBundle mainBundle] loadNibNamed:@"FSFormView" owner:self options:nil] lastObject];
    //set the content size
    float statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGRect viewBounds = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height -statusBarHeight );
    
    contentView.frame = viewBounds;
    
    [scrollView addSubview:contentView];
    //set the content size of the scroll view
    [scrollView setContentSize:CGSizeMake(viewBounds.size.width, viewBounds.size.height)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)leftButtonClicked:(id)sender{
    [scrollView setContentOffset:CGPointMake(0, 60) animated:YES];
}
-(IBAction)rightButtonClicked:(id)sender{
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
@end
