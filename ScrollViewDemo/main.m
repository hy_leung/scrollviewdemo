//
//  main.m
//  ScrollViewDemo
//
//  Created by Ho Yan Leung on 2013-08-20.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FSAppDelegate class]));
    }
}
